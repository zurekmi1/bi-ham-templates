#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <errno.h>
#include "hexdump.h"

struct hexdump *hx;

int packet_raw(void)
{
    struct sockaddr saddr;
    int saddr_size = sizeof(saddr), data_size;

    uint8_t *buffer = (uint8_t *) malloc(65536);
    uint8_t *hexbuffer = (uint8_t *) malloc(65536);

    int sd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, "wlo1", 5);
    if (sd < 0) {
        err(1, "Failed to bind to device.");
    }

    data_size = recvfrom(sd, buffer, 65536, 0, &saddr, (socklen_t*) &saddr_size);
    if (data_size < 0) {
        err(1, "Failed to receive packet.");
    }

    hxd_write(hx, buffer, data_size);
    hxd_flush(hx);
    data_size = hxd_read(hx, hexbuffer, 65535);
    printf("%.*s", data_size, hexbuffer);
    close(sd);
    free(buffer);
    free(hexbuffer);
}

int packet_dgram(void)
{
    struct sockaddr saddr;
    int saddr_size = sizeof(saddr), data_size;

    uint8_t *buffer = (uint8_t *) malloc(65536);
    uint8_t *hexbuffer = (uint8_t *) malloc(65536);

    int sd = socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_ALL));
    setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, "wlo1", 5);
    if (sd < 0) {
        err(1, "Failed to bind to device.");
    }

    data_size = recvfrom(sd, buffer, 65536, 0, &saddr, (socklen_t*) &saddr_size);
    if (data_size < 0) {
        err(1, "Failed to receive packet.");
    }

    hxd_write(hx, buffer, data_size);
    hxd_flush(hx);
    data_size = hxd_read(hx, hexbuffer, 65535);
    printf("%.*s", data_size, hexbuffer);
    close(sd);
    free(buffer);
    free(hexbuffer);
}


int main(int argc, char **argv)
{
    hx = hxd_open(NULL);
    hxd_compile(hx, HEXDUMP_C, HXD_NETWORK);

    printf("socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))\n");
    packet_raw();

    printf("socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_ALL))\n");
    packet_dgram();

    hxd_close(hx);
}

