# pcapexample

This code is an educational example of libpcap usage.
It is based on previously published example by Luis Martin Garcia (program was
originally called simplesniffer), however, the original code was extensively improved and extended, autotools mechanism was added for build.

## Building the code

Standard way can be used:

```
autoreconf -i
./configure
make
```

(Optionally `sudo make install` - this is not necessary to execute the binary.)

## Help

To get help - usage, just execute `./pcapexample -h`.

## Further notes

To allow ordinary users to capture packets from device, it is possible to set "capability":

`sudo setcap "CAP_NET_RAW+eip" pcapexample`

