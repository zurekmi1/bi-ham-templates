/* Example of parsing a raw packet                                       */
/* Author: Tomas Cejka <cejkato2@fit.cvut.cz>, 2020                      */

/* This code is distributed under the GPL License. For more info check:  */
/* http://www.gnu.org/copyleft/gpl.html                                  */

#include <config.h>
#define _GNU_SOURCE
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>

#define MIN(a,b) (((a)<(b))?(a):(b))

/* endien conversion */
#include <arpa/inet.h>

#include <netinet/ether.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include "packets.h"

#define MAXPAYLOADSIZE 1024
static char payload[MAXPAYLOADSIZE];

int parse_packet(const unsigned char *pkt, uint16_t size)
{
    /* /usr/include/net/ethernet.h */
    const struct ether_header *ether_p = NULL;

    /* /usr/include/netinet/ip.h */
    const struct iphdr *ip_p = NULL;

    /* /usr/include/netinet/tcp.h */
    const struct tcphdr *tcp_p = NULL;

    /* protocol constants: /usr/include/netinet/in.h */

    const char *packet = pkt;
    const char *p = packet, *p2 = NULL;

    /* Check the following L3 protocol */
    ether_p = (const struct ether_header *) p;
    if (ntohs(ether_p->ether_type) == ETHERTYPE_IP) {
        printf("The next layer is IP based on ntohs(ether_p->ether_type)\n");
    } else {
        printf("Unknown network protocol (%X)\n", ntohs(ether_p->ether_type));
        return EXIT_FAILURE;
    }

    ip_p = (const struct iphdr *) (ether_p + 1);

    /* Warning - is ip_p inside the buffer? */

    /* Check the following L4 protocol */
    if (ip_p->protocol == IPPROTO_TCP) {
        printf("The next layer is TCP based on ip_p->protocol\n");
    } else {
        printf("Unknown transport protocol (%X)\n", ip_p->protocol);
        return EXIT_FAILURE;
    }

    printf("IP header has %d words, 5 is standard lenght without IP options\n", ip_p->ihl);
    tcp_p = (const struct tcphdr *) (((char *) ip_p) + (ip_p->ihl * 4));

    /* Warning - is ip_p inside the buffer? */

    /* Check the following Application protocol */
    printf("TCP destination port: %d\n", ntohs(tcp_p->dest));
    if (ntohs(tcp_p->dest) == 80) {
        printf("Based on destination port, it is likely HTTP.\n");
    }

    /* Get payload of TCP */
    p = (const char *) (tcp_p + 1);
    if (p < packet) {
        printf("We are after the packet!\n");
        return EXIT_FAILURE;
    }
    uint16_t payload_size = p - packet;

    printf("Payload starts on offset %d\n", payload_size);

    memcpy(payload, p, MIN(payload_size, MAXPAYLOADSIZE - 2));
    payload[MAXPAYLOADSIZE - 1] = 0;

    p = strcasestr(payload, "host: ");
    if (p != NULL) {
        printf("Extracted HTTP Header:\n\t");
        p2 = strchr(p, '\r');
        if (p2 == NULL) {
            p2 = strchr(p, '\n');
        }
        if (p2 != NULL) {
            printf("%.*s\n", p2 - p, p);
        }
    }
    return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
    printf("1. Packet length: %d\n", sizeof(pkt1));
    parse_packet(pkt1, sizeof(pkt1));

    printf("\n2. Packet length: %d\n", sizeof(pkt2));
    parse_packet(pkt2, sizeof(pkt2));

    printf("\n3. Packet length: %d\n", sizeof(pkt3));
    parse_packet(pkt3, sizeof(pkt3));

    return EXIT_SUCCESS;
}

