#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "shared.hpp"

#include <iostream>
using namespace std;

TEST_CASE("Basic example pcap http test") {
    auto monlib = test_monlib_init();
    CHECK(test_monlib_pcap(monlib, "test_data/http.cap"));

    auto stats = monlib_get_stats(monlib.get());
    CHECK(stats.packets == 43);

    //TODO: Check validity
//    SUBCASE("adding to the vector increases it's size") {
//        v.push_back(1);
//        CHECK(v.size() == 6);
//        CHECK(v.capacity() >= 6);
//    }
//    SUBCASE("reserving increases just the capacity") {
//        v.reserve(6);

//        CHECK(v.size() == 5);
//        CHECK(v.capacity() >= 6);
//    }
}
