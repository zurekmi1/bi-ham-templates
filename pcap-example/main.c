/* Example of raw packet sniffer Sniffer using libpcap                   */
/* Author: Tomas Cejka <cejkato2@fit.cvut.cz>, 2020                      */

/* Based on Simple Raw Sniffer by                                        */
/* Author: Luis Martin Garcia. luis.martingarcia [.at.] gmail [d0t] com  */

/* To compile: gcc simplesniffer.c -o simplesniffer -lpcap               */
/* Run as root!                                                          */
/*                                                                       */
/* This code is distributed under the GPL License. For more info check:  */
/* http://www.gnu.org/copyleft/gpl.html                                  */

#include <config.h>
#include <pcap.h>
#include <ctype.h>
#include <string.h>
#include <stdio.h>     /* for printf */
#include <getopt.h>
#include <stdlib.h>

#define MAXBYTES2CAPTURE 2048


/* processPacket(): Callback function called by pcap_loop() everytime a packet */
/* arrives to the network card. This function prints the captured raw data in  */
/* hexadecimal.                                                                */
void processPacket(u_char *arg, const struct pcap_pkthdr* pkthdr, const u_char *packet)
{

    int i=0, *counter = (int *) arg;

    printf("Packet Count: %d\n", ++(*counter));
    printf("Received Packet Size: %d\n", pkthdr->caplen);
    printf("Payload:\n");
    for (i = 0; i < pkthdr->caplen; i++){
        if (isprint(packet[i])) {
            /* If it is a printable character, print it */
            printf("%c ", packet[i]);
        } else {
            printf(". ");
        }

        if ((i%16 == 0 && i!=0) || i == pkthdr->caplen-1) {
            printf("\n");
        }
    }
    return;
}

int main(int argc, char *argv[])
{
    int i=0, count=0;
    int c, digit_optind = 0, listdevs = 0;
    const char *file = NULL, *dev = NULL;
    pcap_t *pcap = NULL;
    char errbuf[PCAP_ERRBUF_SIZE], *device = NULL;
    memset(errbuf, 0, PCAP_ERRBUF_SIZE);

    /* handle command line arguments */
    while (1) {
        int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {
            {"listdevs",  no_argument, 0,  'l' },
            {"file",      required_argument, 0,  'f' },
            {"device",    required_argument, 0,  'd' },
            {"help",    no_argument, 0,  'h' },
            {0,         0,                 0,  0 }
        };

        c = getopt_long(argc, argv, "d:hf:l", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 'f':
            file = optarg;
            break;
        case 'd':
            dev = optarg;
            break;
        case 'h':
            printf("%s\n", PACKAGE_STRING);
            printf("Usage: %s [-dfhl]\n", PACKAGE_NAME);
            printf("\t-d, --device DEVICE\tCapture from network interface card\n");
            printf("\t-f, --file PATH\tOpen PCAP file from PATH\n");
            printf("\t-h, --help\tPrint this help\n");
            printf("\t-l, --listdevs\tPrint list of available devices (for -d argument)\n");
            exit(EXIT_SUCCESS);
        case 'l':
            listdevs = 1;
            break;
        case '?':
            break;

        default:
            printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind < argc) {
        /* unused positional arguments */
        printf("non-option ARGV-elements: ");
        while (optind < argc)
            printf("%s ", argv[optind++]);
        printf("\n");
    }

    if (file && dev) {
        printf("It is not possible to use both 'file' and 'device' arguments.");
        exit(EXIT_FAILURE);
    }

    /* Print list of available devices by '-l' argument */
    if (listdevs) {
        pcap_if_t *alldevsp, *p;

        if ((pcap_findalldevs(&alldevsp, errbuf)) != 0){
            fprintf(stderr, "ERROR: %s\n", errbuf);
            exit(EXIT_FAILURE);
        }
        p = alldevsp;

        printf("%20s\t%s\n", "NAME", "DESCRIPTION");
        while (p) {
            printf("%20s\t%s\n", p->name, p->description ? p->description : "-");
            p = p->next;
        }
        pcap_freealldevs(alldevsp);
        exit(EXIT_SUCCESS);
    }

    /* Check if enough information was provided */
    if (!file && !dev) {
        printf("To run this application, --file or --dev is needed. Use -h / --help for help.\n");
        exit(EXIT_FAILURE);
    }

    printf("Debug: Opening '%s'\n", dev ? dev : file);

    if (dev) {
        /* Open device in promiscuous mode */
        pcap = pcap_open_live(device, MAXBYTES2CAPTURE, 1 /* promisc mode */,
                              512 /* buffer timeout */, errbuf);
    } else {
        /* Open file */
        pcap = pcap_open_offline(file, errbuf);
    }

    /* Check for errors */
    if (pcap == NULL) {
        fprintf(stderr, "ERROR: %s\n", errbuf);
        exit(EXIT_FAILURE);
    }

    /* Loop forever & call processPacket() for every received packet*/
    if (pcap_loop(pcap, -1, processPacket, (u_char *) &count) == -1){
        fprintf(stderr, "ERROR: %s\n", pcap_geterr(pcap) );
        exit(EXIT_FAILURE);
    }

    pcap_close(pcap);
    return EXIT_SUCCESS;
}

