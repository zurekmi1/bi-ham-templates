#ifndef MONLIB_OS_H
#define MONLIB_OS_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void* (*monlib_malloc)(unsigned int);
typedef void (*monlib_free)(void *);

struct monlib_os {
    monlib_malloc alloc;
    monlib_free free;
};


#ifdef __cplusplus
}
#endif

#endif
