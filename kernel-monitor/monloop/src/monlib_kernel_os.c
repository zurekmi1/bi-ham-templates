#include "monlib_kernel_os.h"
#include <linux/slab.h>

void* monlib_kernel_os_malloc(unsigned int size)
{
    return kmalloc(size, GFP_KERNEL);
}

void monlib_kernel_os_free(void *ptr)
{
    kfree(ptr);
}

void monlib_kernel_os_init(struct monlib_os *os_ctx)
{
    os_ctx->alloc = monlib_kernel_os_malloc;
    os_ctx->free = monlib_kernel_os_free;
}
