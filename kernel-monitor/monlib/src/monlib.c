#include "monlib.h"

static int packet_processed = 0;
int monlib_init(struct monlib_ctx *ctx)
{
    return 0;
}

int monlib_process(struct monlib_ctx *ctx, const void *packet, const unsigned int packet_len)
{
    packet_processed++;
    return 0;
}

struct monlib_stats monlib_get_stats(struct monlib_ctx *ctx)
{
    struct monlib_stats stats;
    stats.packets = packet_processed;
    return stats;
}

void monlib_reset(struct monlib_ctx *ctx)
{
    packet_processed = 0;
}
