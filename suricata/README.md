# Suricata task

## Intallation
- https://suricata.readthedocs.io/en/suricata-6.0.0/install.html#binary-packages
- The `jq` program will be handy for working with JSON data.

## Preperation
Download and extract all pcap files from https://github.com/pan-unit42/wireshark-tutorial-Emotet-traffic. Password is "infected". Write detection rules to the `suricata.rules` file.

## Running
```bash
suricata -r PCAP_FOLDER -c SURICATA_YAML_FILE -l LOG_DIR -S SURICATA_RULES_FILE -T # for testing the configuration
suricata -r PCAP_FOLDER -c SURICATA_YAML_FILE -l LOG_DIR -S SURICATA_RULES_FILE
jq -c 'select(.event_type == "alert")' eve.json > eve-alerts.json
```

## Tips
- Use the jq command to filter the EVE output for alerts only.
- Log file `fast.log` contains simplified alerts.
